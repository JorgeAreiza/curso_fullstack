$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('#exampleModal').on('show.bs.modal', function (e) {
        $('#BtnInformacion').removeClass('btn-primary');
        $('#BtnInformacion').addClass('btn-secondary');
        $('#BtnInformacion').prop('disabled', true);
    })

    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('El modal contacto se está mostrando');

        $('#exampleModal').removeClass('btn-outline-success');
        $('#exampleModal').addClass('btn-primary');
        $('#exampleModal').prop('disabled', true);

    });
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('El modal contacto se mostró');
    });
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('El modal contacto se oculta');
    });
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('El modal contacto se ocultó');
        $('#exampleModal').prop('disabled', false);
    });
});


